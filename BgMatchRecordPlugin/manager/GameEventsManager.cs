﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HearthDb.Enums;

using Hearthstone_Deck_Tracker.Enums;
using Hearthstone_Deck_Tracker.Hearthstone;
using Hearthstone_Deck_Tracker.API;
using System.Windows.Controls;

namespace BgMatchRecordPlugin
{
    public class GameEventsManager
    {
        public static View _view;

        public static SimpleWindow _simpleWindow { get; internal set; }

        public static void registerEvents() {
            // Triggered upon startup and when the user ticks the plugin on            
            GameEvents.OnGameStart.Add(OnGameStart);
            GameEvents.OnInMenu.Add(OnInMenu);
            GameEvents.OnTurnStart.Add(OnTurnStart);
            GameEvents.OnGameEnd.Add(OnGameEnd);
        }


        internal static void OnInMenu()
        {
            if (!Core.Game.Spectator)
            {
                _view.SetisBanned(Core.Game.Player.Name, Core.Game.CurrentRegion.ToString() + " in menu");
                _simpleWindow.SetisBanned(Core.Game.Player.Name, Core.Game.CurrentRegion.ToString() + " in menu-simple");

                if (!Core.OverlayCanvas.Children.Contains(_simpleWindow))
                {
                    Core.OverlayCanvas.Children.Add(_simpleWindow);
                    Canvas.SetZIndex(_simpleWindow, 100);
                }
            }
        }

        internal static void OnGameStart()
        {
            if (!Core.Game.Spectator)
            {
                _view.SetisBanned(Core.Game.Player.Name, Core.Game.CurrentRegion.ToString() + " game start");
                _simpleWindow.SetisBanned(Core.Game.Player.Name, Core.Game.CurrentRegion.ToString() + " game start-simple");

                if (!Core.OverlayCanvas.Children.Contains(_simpleWindow))
                {
                    Core.OverlayCanvas.Children.Add(_simpleWindow);
                    Canvas.SetZIndex(_simpleWindow, 100);
                }
            }
        }
        internal static void OnTurnStart(ActivePlayer player)
        {
        }

        internal static void OnGameEnd()
        {
        }

        internal static bool isInBgMenu()
        {
            if (Core.Game.CurrentMode != Hearthstone_Deck_Tracker.Enums.Hearthstone.Mode.BACON) return false;
            else return true;
        }
        internal static bool isInBgMode()
        {
            if (Core.Game.CurrentGameMode != GameMode.Battlegrounds) return false;
            else return true;
        }
        internal static bool isInGameplayMode()
        {
            if (Core.Game.CurrentMode != Hearthstone_Deck_Tracker.Enums.Hearthstone.Mode.GAMEPLAY) return false;
            else return true;
        }
        internal static bool isInGameHub()
        {
            if (Core.Game.CurrentMode != Hearthstone_Deck_Tracker.Enums.Hearthstone.Mode.HUB) return false;
            else return true;
        }
    }
}
