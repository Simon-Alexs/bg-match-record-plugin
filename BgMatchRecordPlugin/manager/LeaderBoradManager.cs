﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;

namespace BgMatchRecordPlugin.manager
{
    public class Leaderboard
    {
        public string AccountId { get; set; }
        public int Rank { get; set; }
        public int Rating { get; set; }

        public static List<Leaderboard> GetLeaderboard(string region)
        {
            List<Leaderboard> leaderboardList = new List<Leaderboard>();
            WebClient client = new WebClient();

            // 获取第一页数据
            var adress = @"https://playhearthstone.com/en-us/api/community/leaderboardsData?region=" + region + "&leaderboardId=battlegroundsduo";
            var strPageCode = client.DownloadString(adress);
            dynamic dobj = JsonConvert.DeserializeObject<dynamic>(strPageCode);
            resolveData(dobj, leaderboardList);
            // 根据总页数，获取所有数据
            var totalPages = dobj["leaderboard"]["pagination"]["totalPages"];
            for (int i = 2; i <= totalPages; i++)
            {
                var pageCode = client.DownloadString(adress + "&page=" + i);
                dynamic jsonObj = JsonConvert.DeserializeObject<dynamic>(pageCode);
                resolveData(jsonObj, leaderboardList);
            }
            /*leaderboardList.Where(d => d.AccountId.Contains(nickname)).Select(s => s.Rank).FirstOrDefault();*/
            return leaderboardList;
        }

        private static void resolveData(dynamic dobj, List<Leaderboard> leaderboardList)
        {
            var rows = dobj["leaderboard"]["rows"];
            for (int i = 0; i < rows.Length; i++)
            {
                Leaderboard leaderboard = new Leaderboard();
                leaderboard.AccountId = rows[i]["accountid"];
                leaderboard.Rank = rows[i]["rank"];
                leaderboard.Rating = rows[i]["rating"];
                leaderboardList.Add(leaderboard);
            }
        }
    }
}
