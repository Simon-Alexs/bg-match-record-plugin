﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Hearthstone_Deck_Tracker.Plugins;
using Hearthstone_Deck_Tracker.API;
using MahApps.Metro.Controls;

namespace BgMatchRecordPlugin
{
    public class BgMatchRecordPlugin : IPlugin
    {
        private View _view;
        private SimpleWindow _simpleWindow;

        public string Name => "BgMatchRecordPlugin";

        public string Description => "Shows your reached Ranks, Banned Tribes and best Heroes. Soon there will be even more, still in Progress :)";

        public string ButtonText => "Settings";

        public string Author => "SimonAlexs";

        public Version Version => new Version(0, 0, 0, 1);

        public MenuItem MenuItem => CreateMenu();


        private MenuItem CreateMenu()
        {
            MenuItem m = new MenuItem { Header = "BgMatchRecordPlugin" };
            return m;
        }

        public void OnButtonPress()
        {

        }

        public void OnLoad()
        {
            _view = new View();
            _simpleWindow = new SimpleWindow();
            GameEventsManager._view = _view;
            GameEventsManager._simpleWindow = _simpleWindow;
           /* MountOverlay();*/
            GameEventsManager.registerEvents();


            if (!Core.Game.Spectator)
            {
                _view.SetisBanned(Core.Game.Player.Name, Core.Game.CurrentRegion.ToString() + " on load");
            }
        }

        public void OnUnload()
        {
        }

        public void OnUpdate()
        {
            if (Core.Game.Spectator) {
                return;
            }
            if (!Core.OverlayCanvas.Children.Contains(_simpleWindow))
            {
                Core.OverlayCanvas.Children.Add(_simpleWindow);
                Canvas.SetZIndex(_simpleWindow, 100);
            }
        }


        private void MountOverlay()
        {
            StackPanel BgsTopBar = (StackPanel) Core.OverlayWindow.FindName("BgsTopBar");
            /*BgsTopBar.Children.Insert(1, _view);*/
        }


        private void UnmountOverlay()
        {
            StackPanel BgsTopBar = (StackPanel) Core.OverlayWindow.FindName("BgsTopBar");
            BgsTopBar.Children.Remove(_view);
        }
    }
}
